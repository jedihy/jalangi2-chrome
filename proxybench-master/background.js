chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  console.log(sender.tab ?
              "from a content script:" + sender.tab.url :
              "from the extension");
  console.log(request.code);
  if (request.cmd != undefined) {
    if (request.cmd == "showresult") {
      // chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      //   for(var i = 0; i<tabs.length;i++) {
      //       chrome.tabs.executeScript(tabs[i].id, {code:'var script = document.createElement("script");script.innerHTML = "J$.Results.showHideResults();";document.body.appendChild(script);'});
      //   }
      // });
        chrome.tabs.executeScript({code:'var script = document.createElement("script");script.innerHTML = "J$.Results.showHideResults();";document.body.appendChild(script);'});
    } else if (request.cmd == "resetproxy") {
      var config = {
          mode: "system"
        };
      chrome.proxy.settings.set({value: config, scope: 'regular'}, function() {});
      console.log("set regular proxy!");
    }
  } else {
      var config = {
        mode: "fixed_servers",
        rules: {
          singleProxy: {
            scheme: "http",
            host: "127.0.0.1",
            port: 9999
          },
          bypassList: ["*.google.com"]
        }
      };

      chrome.proxy.settings.set({value: config, scope: 'regular'}, function() {});
      console.log("proxy settle");
  }
});