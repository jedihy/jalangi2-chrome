// JALANGI DO NOT INSTRUMENT
(function (sandbox) {
    function SMemory(x) {
        this.val = x;
    }

    SMemory.genSMemory = function(x) {
        if (SMemory.isShadowed(x)) {
            return x;
        } else {
            return new SMemory(x);
        }
    }

    SMemory.getVal = function(v) {
        return SMemory.isShadowed(v) ? v.val : v;
    }

    SMemory.isShadowed = function(x) {
        return x instanceof SMemory;
    }

    function Taint() {
        this._builtinFunctionNames = ["unescape", "encodeURIComponent", "constructor", "toExponential", "toFixed", "toLocaleString", "toPrecision", "toString", "valueOf", "toSource", "toString", "valueOf", "charAt", "charCodeAt", "concat", "indexOf", "lastIndexOf", "localeCompare", "length", "match", "replace", "search", "slice", "split", "substr", "substring", "toLocaleLowerCase", "toLocaleUpperCase", "toLowerCase", "toString", "toUpperCase", "valueOf", "anchor", "big", "blink", "bold", "fixed", "fontcolor", "fontsize", "italics", "link", "small", "strike", "sub", "sup", "concat", "every", "filter", "forEach", "indexOf", "join", "lastIndexOf", "map", "pop", "push", "reduce", "reduceRight", "reverse", "shift", "slice", "some", "toSource", "sort", "splice", "toString", "unshift", "Date", "getDate", "getDay", "getFullYear", "getHours", "getMilliseconds", "getMinutes", "getMonth", "getSeconds", "getTime", "getTimezoneOffset", "getUTCDate", "getUTCDay", "getUTCFullYear", "getUTCHours", "getUTCMilliseconds", "getUTCMinutes", "getUTCMonth", "getUTCSeconds", "getYear", "setDate", "setFullYear", "setHours", "setMilliseconds", "setMinutes", "setMonth", "setSeconds", "setTime", "setUTCDate", "setUTCFullYear", "setUTCHours", "setUTCMilliseconds", "setUTCMinutes", "setUTCMonth", "setUTCSeconds", "setYear", "toDateString", "toGMTString", "toLocaleDateString", "toLocaleFormat", "toLocaleString", "toLocaleTimeString", "toSource", "toString", "toTimeString", "toUTCString", "valueOf"];
        
        this._builtinClassNames = ["HTMLDocument", "Array", "Date", "String"];

        this._taintStack = [];
        this._taintInfo = [];

        this.invokeFunPre = function (iid, f, base, args, isConstructor, isMethod, functionIid, functionSid) {
            
            let isTaint = false;

            if (SMemory.isShadowed(f)) {
                f = SMemory.getVal(f);
                isTaint = true;
            }

            if (SMemory.isShadowed(base)) {
                base = SMemory.getVal(base);
                isTaint = true;
            }

            let n_args = [];

            for(let i = 0; i < args.length; i++) {
                if (SMemory.isShadowed(args[i])) {
                    n_args.push(SMemory.getVal(args[i]));
                    isTaint = true;
                } else {
                    n_args.push(args[i]);
                }
            }

            this._taintStack.push(isTaint);

            return {f: f, base: base, args: n_args, skip: false};
        };

        this.invokeFun = function (iid, f, base, args, result, isConstructor, isMethod, functionIid, functionSid) {
            
            if (this._taintStack.pop()) {
                result = SMemory.genSMemory(result);
            }

            if (f.name == "taint") {
                result = SMemory.genSMemory(result);
            } else if (f.name == "isTaint") {
                result = SMemory.isShadowed(result);
            }

            return {result: result};
        };

        this.getFieldPre = function (iid, base, offset, isComputed, isOpAssign, isMethodCall) {
            let toTaint = SMemory.isShadowed(base) || SMemory.isShadowed(offset);
            this._taintStack.push(toTaint);

            base = SMemory.getVal(base);
            offset = SMemory.getVal(offset);
            
            return {
                base: base,
                offset: offset, 
                skip: false 
            };
        };

        this.getField = function (iid, base, offset, val, isComputed, isOpAssign, isMethodCall) {
            if (this._taintStack.pop()) {
                val = SMemory.genSMemory(val);
            }

            return {result: val};
        };

        this.putFieldPre = function (iid, base, offset, val, isComputed, isOpAssign) {
            let isShadowed = SMemory.isShadowed(base) || SMemory.isShadowed(offset) || SMemory.isShadowed(val);            
            offset = SMemory.getVal(offset);
            base = SMemory.getVal(base);

            if (isShadowed) {
                this._taintInfo.push("" + base.constructor.name + "." + offset);
            }

            return {base: base, offset: offset, val: val, skip: false};
        };

        this.write = function (iid, name, val, lhs, isGlobal, isScriptLocal) {
            var _name = SMemory.getVal(name);
            let isShadowed =  SMemory.isShadowed(val);
            if (isShadowed)
                this._taintInfo.push("" + _name);
            return {result: val};
        };

        this.binaryPre = function (iid, op, left, right, isOpAssign, isSwitchCaseComparison, isComputed) {
            var isTaint = SMemory.isShadowed(left) || SMemory.isShadowed(right);
            if (op == "|" && !SMemory.isShadowed(left) && left)
                isTaint = false;
            if (op == "&" && !SMemory.isShadowed(left) && !left)
                isTaint = false;
            if (op == "^" && left === right)
                isTaint = false;

            this._taintStack.push(isTaint);
            left = SMemory.getVal(left);
            right = SMemory.getVal(right);
            
            return {op: op, left: left, right: right, skip: false};
        };

        this.binary = function (iid, op, left, right, result, isOpAssign, isSwitchCaseComparison, isComputed) {

            if (this._taintStack.pop()) {
                result = SMemory.genSMemory(result);
            }
            // console.log("binaryPre left = " + left + " right = " + right + " result = " + result);
            return {result: result};
        };

        this.unaryPre = function (iid, op, left) {
            this._taintStack.push(SMemory.isShadowed(left));
            left = SMemory.getVal(left);
            return {op: op, left: left, skip: false};
        };

        this.unary = function (iid, op, left, result) {
            
            if (this._taintStack.pop()) {
                result = SMemory.genSMemory(result);
            }

            return {result: result};
        };

        this.endExecution = function () {
            console.log(J$.smap);
            let __taintInfo = this._taintInfo;
            var ret = "";
            ret += "Found Tainted Variables:\n";
            for (var idx in __taintInfo) {
                console.log(__taintInfo[idx]);
                ret += __taintInfo[idx] + "\n";
            }
            return ret;
        };
    }

    sandbox.analysis = new Taint();
    console.log('Taint Analysis is Active');
})(J$);